# Cancer tumor modeling with Cellular Automata

## Ideas

*Goal* Study impact of $O_2$ concentration on cell growth and the mutual interaction between tumour cells and blood vessels through angiogenesis.

- Cell quiescence
- 3D cellular automaton
    - Three.js
- Angiogenesis
    - Blood cells
- States
    - Healthy
    - Tumourous
    - Dead 
- Events
    - Apoptosis
    - Proliferation (division)
    - Migration
    - Quiescence 
    - Senescence
    
## Rules

- There is a source of $O_2$ as a frame around the world.
- Cells consumes $O_2$
- The less $O_2$ there is, the less the cells divide.
- $O_2$ diffuses on all cells at rate $\nabla$ 
- The $O_2$ diffuse faster in blood cells at rate $\nabla_{blood}$
- Cell ages
- Aged cells enters in senescence
- Senescent cell does not divide
- Cells divide at rates $\delta_{tumorous}$, $\delta_{healthy}$
  - $\delta_{tumorous} > \delta_{healthy}$
  - A cells can proliferate only if an empty space is available in its neighborhood
  - The daughter cell is placed on any randomly chosen empty neighbor cell 
- Cells enter in apoptosis with probability $\alpha_{tumorous}$, $\alpha_{healthy}$
  - $\alpha_{tumorous} < \alpha_{healthy}$
- A tumour cell surrounded by other cells become quiescent
- A tumour cell wander as in brownian motion
- A tumour cell emits an angiogenesis signal if in low $[O_2]$
- Blood cells reproduce towards the gradient of angiogenesis signal concentration (?)

## Team members
 
- Kader Houssein
- Samuel Ortion
- Naïa Périnelle  (**Team leader**)

## Former projects

- Effect of chemotherapy
- Impact of immune cells (lymphocytes)
- Effect of growth factors

## Motion of patches

A netlogo model has been written to play with moving patches.

For the moment, a cheaty solution allows to make a single patch move.
![a single moving patch in NetLogo](./media/moving-patch.gif)
