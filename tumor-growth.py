"""
Cellular automaton - Tumor growth

The simulation studies the evolution of a tumor
"""

from cellularautomata import CountType, GuiCA
from random import random, choices

APOPTOSE_RATE_TUMOROUS: float = 0.2
APOPTOSE_RATE_HEALTHY: float = 0.8
DIVISION_RATE_TUMOROUS: float = 0.8
DIVISION_RATE_HEALTHY: float = 0.2
DIFFUSION_RATE: float = 0.7


def tumor_growth(cell, neighbors: list):
    category, time = cell
    match category:
        case "Healthy":
            return cell

        case "Tumorous":
            return cell

        case "Empty":
            try:
                # Division
                nb_healthy = CountType(neighbors, "Healthy")
                nb_tumorous = CountType(neighbors, "Tumorous")
                staying_proba = 1 - (DIVISION_RATE_TUMOROUS + DIVISION_RATE_HEALTHY)
                other_neighbors = 8 - (nb_healthy + nb_tumorous)
                weights = [
                    nb_healthy * DIVISION_RATE_HEALTHY + 1,
                    nb_tumorous * DIVISION_RATE_TUMOROUS + 1,
                    other_neighbors * staying_proba + 1,
                ]
                new_category = choices(["Healthy", "Tumorous", "Empty"], weights=weights, k=1)[0]
            except Exception as e:
                print(e)
            return (new_category, None)
          
        case 'Quiescent':
            return cell
        case _:
            return cell


cellcolors = {  # color assigned to cells in their initial states.
    ("Empty", None): "cyan",
    ("Healthy", None): "beige",
    ("Tumorous", None): "red",
    ("Quiescent", None): "pink",
}

GuiCA(tumor_growth, cellcolors)
